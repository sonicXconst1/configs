#!/bin/bash

cp $HOME/.config/nvim/init.vim nvim/init.vim
cp $HOME/.config/xmobar/xmobar.config xmobar/xmobar.config
